Source
https://wrapbootstrap.com/theme/material-admin-responsive-angularjs-WB011H985

Features

    Comes with Light and Dark Skins
    Built with material design guidelines
    jQuery and Angular variants
    Material Design Colors, Icons and Animations
    All pages are HTML5 validated and neatly coded.
    Fully Responsive design, supported for desktops, laptops, smartphones and tablets.
    Bower Package Manager
    Uses LESS and Grunt.
    Built on latest Twitter Bootstrap.
    Complete User Interface elements and components.
    Event calendar with 2 different presentations
    Variety of form Components (Validation, Color picker, Date picker, Toggler, select, etc..)
    Retina display ready Icons and Images
    Media components for better Multimedia (HTML5 Video/Audio Players, Modern Lightbox, Image Carousel )
    Sample pages included such as Profile, Messages, Listview, Login etc.

Credits

    Bootstrap - http://getbootstrap.com
    jQuery - http://jquery.com
    AngularJs - https://angularjs.org/
    Bower - http://bower.io/
    GruntJs - http://gruntjs.com
    Angular Loading Bar - http://chieffancypants.github.io/angular-loading-bar/
    Angular UI Router - https://github.com/angular-ui/ui-router
    OC Lazy Load for AngularJs - https://github.com/ocombe/ocLazyLoad
    Animate.css - http://daneden.github.io/animate.css/
    Auto Size Textarea - http://www.jacklmoore.com/autosize/
    Date Time Picker - http://eonasdan.github.io/bootstrap-datetimepicker/
    Bootstrap Select - http://silviomoreto.github.io/bootstrap-select/
    Bootstrap Wizard - http://vadimg.com/twitter-bootstrap-wizard-example/
    Chosen - http://harvesthq.github.io/chosen/
    EasyPieChart - http://rendro.github.io/easy-pie-chart/
    Color Picker - http://acko.net/blog/farbtastic-jquery-color-picker-plug-in/
    Simple File Input - http://jasny.github.com/bootstrap/javascript/#fileinput
    Flot Chart - http://www.flotcharts.org/
    Full Calendar - http://fullcalendar.io
    Input Mask - http://blog.igorescobar.com
    Material Design Icon - http://zavoloklom.github.io/material-design-iconic-font/icons.html
    MediaELement JS - http://mediaelementjs.com/
    Moment JS - http://momentjs.com/
    Malihu Content Scroller - http://manos.malihu.gr/jquery-custom-content-scroller/
    NoUiSlider - http://refreshless.com/nouislider/
    Simple Weather - http://simpleweatherjs.com/
    SparkLine Chart - http://omnipotent.net/jquery.sparkline/
    SummerNote - http://summernote.org
    Waves - https://github.com/fians/Waves
    Roboto Font - https://www.google.com/fonts/specimen/Roboto
    Satisfy Font - https://www.google.com/fonts/specimen/Satisfy
    Shadow Light Font - https://www.google.com/fonts/specimen/Shadows+Into+Light
    ng-table - http://ng-table.com/
    SweetAlert2 - https://limonte.github.io/sweetalert2/
    DropZone - http://www.dropzonejs.com/
    DataTable - https://datatables.net/

More by rushenn